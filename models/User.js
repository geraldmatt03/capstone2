// [SECTION] Dependencies and Modules
	const mongoose = require("mongoose");

// [SECTION] Blueprint Schema
	const userSchema = new mongoose.Schema({
		email: {
			type: String,
			required: [true, 'Email is Required']
		},
		password: {
			type: String,
			required: [true, 'Password is Required']
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		firstName: {
			type: String,
			required: [true, 'First Name is Required']
		},
		lastName:{
			type: String,
			required: [true, 'Last Name is Required']
		},
		mobileNo:{
			type: String,
			required: [true, 'Mobile No is Required']
		},
		completeAddress:{
			type: String,
			required: [true, 'Complete Address is Required']
		}
	});

// [SECTION] Model
	const User = mongoose.model('User', userSchema);
	module.exports = User;