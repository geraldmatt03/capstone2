// [SECTION] Dependencies and Modules
	const mongoose = require("mongoose");
	const ObjectID = mongoose.Schema.Types.ObjectId;

// [SECTION] Blueprint Schema
	const cartSchema = new mongoose.Schema({
		user: {
			type: ObjectID,
			required: [true, 'UserId is Required'],
			ref: 'User'
		},

		products: [{
			productId: {
				type: ObjectID,
				required: [true, 'ProductId is Required'],
				ref: 'Product'
			},
			name: {
				type: String
			},
			price: {
				type: Number
			},
			quantity: {
				type: Number,
				required: [true, 'Quantity is Required'],
				default: 1,
				min: 1
			}
		}],

		totalAmount: {
			type: Number,
			required: [true, 'Total Amount is Required'],
			default: 0
		},

		isPaid: {
			type: Boolean,
			default: false
		},
		
		createdOn: {
			type: Date,
			default: new Date()
		}
	});

// [SECTION] Model
	const Cart = mongoose.model("Cart", cartSchema);
	module.exports = Cart;