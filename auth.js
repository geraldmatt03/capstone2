// [SECTION] Dependencies and Module
	const jwt = require('jsonwebtoken');
	const secret = "ecommerceAPI";

// [SECTION] Token Creation
	module.exports.createAccessToken = (user) => {
		const data = {
			id: user._id,
			isAdmin: user.isAdmin
		};

		return jwt.sign(data, secret, {});
	};

// [SECTION] Login Verification
	module.exports.verify = (req, res, next) => {
		console.log(req.headers.authorization);
		let token = req.headers.authorization;

		if (typeof token === "undefined") {
			return res.send({auth: "Failed. No Token"});
		} else {
			console.log(token);

			token = token.slice(7, token.length);

			console.log(token);

			jwt.verify(token, secret, (err, decodedToken) => {
				if (err) {
					return res.send({
						auth: 'Failed',
						message: err.message
					});
				} else {
					console.log(decodedToken);
					req.user = decodedToken;

					next();
				};
			});
		};
	};

// [SECTION] Admin Verification
	module.exports.verifyAdmin = (req, res, next) => {
		if (req.user.isAdmin) {
			next();
		} else {
			return res.send({
				auth: 'Failed',
				message: 'Action Forbidden'
			});
		};
	};