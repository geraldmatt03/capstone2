// [SECTION] Packages and Dependencies
	const express = require("express");
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const cors = require("cors");
	const userRoutes = require('./routes/users');
	const productRoutes = require('./routes/products');
	const orderRoutes = require('./routes/orders');
	const cartRoutes = require('./routes/carts');

// [SECTION] Server Setup
	const app = express();
	app.use(express.json());
	app.use(cors());
	dotenv.config();
	const port = process.env.PORT;
	const acct = process.env.CONNECTION_STRING;

// [SECTION] Application Routes
	app.use('/users', userRoutes);
	app.use('/products', productRoutes);
	app.use('/orders', orderRoutes);
	app.use('/carts', cartRoutes);

// [SECTION] Database Connection
	mongoose.connect(acct);
	let dbStatus = mongoose.connection;
	dbStatus.on('open', () => console.log('Database is connected'));

// [SECTION] Gateway Response
	app.get('/', (req, res) => {
		res.send("Welcome to Gerald's ECommerce APP");
	});
	app.listen(port, () => console.log(`Server is running on port ${port}`));