// [SECTION] Dependencies and Modules
	const User = require('../models/User');
	const Product = require('../models/Product');
	const Order = require('../models/Order');
	const Cart = require('../models/Cart')
	const auth = require('../auth');

// [SECTION] Functionality - CREATE
	module.exports.addToCart = async (req, res) => {
		console.log(req.user.id);
		console.log(req.body);

		const user = req.user.id;
		const {productId, quantity} = req.body;

		if (req.user.isAdmin) {
			return res.send({message: 'Action Denied'});
		}

		const cart = await Cart.findOne({user});
		const product = await Product.findOne({_id: productId})

		if(!product){
			return res.status(404).send({message: 'No Product Found'})
		}

		const price = product.price;
		const name = product.name;

		if (cart) {
			const productIndex = cart.products.findIndex((product) => product.productId === productId)

			if (productIndex > -1){
				let item = cart.products[productIndex];
				item.quantity += quantity;

				cart.totalAmount = cart.products.reduce((acc, curr) => {
					return acc + curr.quantity * curr.price;
				}, 0)

				cart.products[productIndex] = item;
				await cart.save();
				res.status(200).send(cart);
			} else {
				cart.products.push({productId, name, quantity, price});
				cart.totalAmount = cart.products.reduce((acc, curr) => {
					return acc + curr.quantity * curr.price
				},0)
			}

			await cart.save();
			res.status(200).send(cart);
		} else {
			const newCart = await Cart.create({
				user,
				products: [{productId, name, quantity, price}],
				totalAmount: quantity * price
			});
			return res.status(201).send(newCart);
		}
	};

// [SECTION] Functionality - RETRIEVE
	module.exports.cart = async (req, res) => {
		console.log(req.user.id);

		const user = req.user.id;

		const cart = await Cart.findOne({user});
		if(cart && cart.products.length > 0){
			res.status(200).send(cart);
		} else {
			res.send(false);
		}
	};

// [SECTION] Functionality - UPDATE
	module.exports.checkOut = async (req, res) => {
		console.log(req.params.id);

		const user = req.user.id;

		let payment = {
			isPaid: true
		}

		return Cart.findByIdAndUpdate(req.params.id, payment).then(archived => res.send(true)).catch(error => res.send(false));
	}

// [SECTION] Functionality - DELETE
	module.exports.deleteItem = async (req, res) => {
		console.log(req.user.id);
		console.log(req.params.id);

		const user = req.user.id;
		const productId = req.params.id;

		let cart = await Cart.findOne({user});

		const productIndex = cart.products.findIndex((product) => product.productId == productId);

		if (productIndex > -1){
			let product = cart.products[productIndex];
			cart.totalAmount -= product.quantity * product.price;

			if (cart.totalAmount < 0){
				cart.totalAmount = 0;
			}
			cart.products.splice(productIndex, 1);
			cart.totalAmount = cart.products.reduce((acc, curr) => {
				return acc + curr.quantity * curr.price;
			},0)
			cart = await cart.save();

			res.status(200).send(cart);
		} else {
			res.status(404).send({message: "product not found"});
		}
	}