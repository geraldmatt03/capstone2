// [SECTION] Dependencies And Modules
	const Product = require('../models/Product');

// [SECTION] Functionality - CREATE
	module.exports.createProduct = (req, res) => {
		console.log(req.body);

		let newProduct = new Product({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			category: req.body.category
		});

		if (req.body.name !== '' && req.body.description !== '' && req.body.price !== '' && req.body.category !== '') {
			return newProduct.save().then(savedProduct => res.status(201).send(savedProduct)).catch(error => res.send(error));
		} else {
			return res.status(500).send({message: 'Error in Adding New Product. Please Complete All Details.'})
		}
	};
// [SECTION] Functionality - RETRIEVE
	module.exports.getAllProducts = (req, res) => {
		return Product.find({}).then(result => res.send(result))
	};

	module.exports.getSingleProduct = (req, res) => {
		console.log(req.params.id);

		return Product.findById(req.params.id).then(result => res.send(result)).catch(error => res.send(error));
	};

	module.exports.getAvailableProducts = (req, res) => {
		return Product.find({isActive: true}).then(result => res.send(result))
	};

	module.exports.categoryMouse = (req, res) => {
		return Product.find({category: "mouse"}).then(result => res.send(result))
	};

	module.exports.categoryKeyboard = (req, res) => {
		return Product.find({category: "keyboard"}).then(result => res.send(result))
	};

	module.exports.categoryAudio = (req, res) => {
		return Product.find({category: "audio"}).then(result => res.send(result))
	};
	
	module.exports.categoryAccessories = (req, res) => {
			return Product.find({category: "accessories"}).then(result => res.send(result))
		};

// [SECTION] Functionality - UPDATE
	module.exports.updateProduct = (req, res) => {
		console.log(req.params.id);
		console.log(req.body);

		let pName = req.body.name;
		let pDescription = req.body.description;
		let pCost = req.body.price;
		let pCategory = req.body.category;

		let updatedProduct = {
			name: pName,
			description: pDescription,
			price: pCost,
			category: pCategory
		};

		return Product.findByIdAndUpdate(req.params.id, updatedProduct).then(productUpdate => {
			if (pName !== '' && pDescription !== '' && pCost !== '' && pCategory !== '') {
				return res.send({message: 'Product Updated Successfully'});
			} else {
				return res.send({message: 'Please make sure all credentials are filled in'});
			};
		}).catch(error => res.send({message: 'Failed to Update Product'}));
	};

	module.exports.archiveProduct = (req, res) => {
		console.log(req.params.id);

		let archives = {
			isActive: false
		};

		return Product.findByIdAndUpdate(req.params.id, archives).then(archived => res.send(true)).catch(error => res.send(false));
	};

	module.exports.reactivateProduct = (req, res) => {
		console.log(req.params.id);

		let reactivate = {
			isActive: true
		};

		return Product.findByIdAndUpdate(req.params.id, reactivate).then(reactivate => res.send(true)).catch(error => res.send(false));
	};

// [SECTION] Functionality - DELETE
	module.exports.deleteProduct = (req, res) => {
		console.log(req.params.id);

		return Product.findByIdAndRemove(req.params.id).then(data => {
			if(!data){
				res.status(404).send({message: 'Cannot Delete Product'});
			} else {
				res.send({message: 'Product Deleted Successfully'});
			}
		}).catch(err => res.status(500).send({message: 'Could Not Delete Product'}))
	};