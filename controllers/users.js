// [SECTION] Dependencies and Modules
	const User = require('../models/User');
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth');

// [SECTION] Environment Setup
	dotenv.config();
	const salt = Number(process.env.SALT);
	
// [SECTION] Functionalities - CREATE
	module.exports.registerUser = (req, res) => {
		console.log(req.body);

		let email = req.body.email;
		let password = req.body.password;
		let firstName = req.body.firstName;
		let lastName = req.body.lastName;
		let mobileNo = req.body.mobileNo;
		let completeAddress = req.body.completeAddress;

		console.log(email);

		if (email !== '' && password !== '' && firstName !== '' && lastName !== '' && mobileNo !== '' && completeAddress !== '') {
				User.findOne({email: email}).then(user => {
				if (user) {
					res.send({message: `This email already exists. Pick another to continue`});
				} else {
					let newUser = new User({
						email: email,
						password: bcrypt.hashSync(password, salt),
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						completeAddress: completeAddress
					});
					newUser.save((err, savedUser) => {
						if (err) {
							return res.status(500).send(false);
						} else {
							return res.status(201).send(true);
						};
					});
				};
			}).catch(error => res.send(error));
		} else {
			return res.status(500).send('Please Fill in All Required Fields');
		}


	};

	module.exports.loginUser = (req, res) => {
		console.log(req.body);

		User.findOne({email: req.body.email}).then(correctEmail => {
			if (correctEmail === null) {
				return res.send({message: `Incorrect Email Address`});
			} else {
				const isPasswordSame = bcrypt.compareSync(req.body.password, correctEmail.password);
				console.log(isPasswordSame);

				if (isPasswordSame) {
					return res.send({accessToken: auth.createAccessToken(correctEmail)});
				} else {
					return res.send(false);
				};
			};
		}).catch(error => res.send(error));
	};


// [SECTION] Functionalities - RETRIEVE
	module.exports.getAllUsers = (req, res) => {
		return User.find({}).then(result => res.send(result)).catch(error => res.send("Can't Retrieve Users"));
	};

	module.exports.getAllAdmin = (req, res) => {
		return User.find({isAdmin: true}).then(result => res.send(result)).catch(error => res.send(error));
	};

	module.exports.getAllCustomers = (req, res) => {
		return User.find({isAdmin: false}).then(result => res.send(result)).catch(error => res.send(error));
	};

	module.exports.getUserDetails = (req, res) => {
		console.log(req.user);
		User.findById(req.user.id).then(result => res.send(result)).catch(err => res.send(err));
	};

// [SECTION] Functionalities - UPDATE
	module.exports.setAdmin = (req, res) => {
		console.log(req.params.id);

		let update = {
			isAdmin: true
		};

		return User.findByIdAndUpdate(req.params.id, update).then(update => res.send(`User ${req.params.id} has been set to an Administrator`)).catch(error => res.send(error));
	};

	module.exports.updateUser = (req, res) => {
		console.log(req.user.id);
		console.log(req.body);

		let firstName = req.body.firstName;
		let lastName = req.body.lastName;
		let mobileNo = req.body.mobileNo;
		let completeAddress = req.body.completeAddress;

		let update = {
			firstName: firstName,
			lastName: lastName,
			mobileNo: mobileNo,
			completeAddress: completeAddress
		};
		
		if (firstName !== ''&& lastName !== ''&& mobileNo !== ''&& completeAddress !== '') {
			return User.findByIdAndUpdate(req.user.id, update).then(update => res.send(`User's details has been updated.`)).catch(error => res.send(error));
		} else {
			return res.send('Please Fill in All Required Details');
		}
		
	};

	module.exports.updatePassword = (req, res) => {
		console.log(req.user.id);
		console.log(req.body);

		let oldPass = req.body.oldPassword;
		let newPass = req.body.newPassword;

		let update = {
			password: bcrypt.hashSync(newPass,salt)
		};

		User.findOne({_id: req.user.id}).then(sameId => {
			if (sameId === null) {
				return res.send('No user found');
			} else {
				const isOldPassSame = bcrypt.compareSync(oldPass, sameId.password);
				const isOldPassSameNewPass = bcrypt.compareSync(newPass, sameId.password);
				console.log(isOldPassSame);

				if(isOldPassSame === false){
					return res.send('Old Password is incorrect');
				} else if (isOldPassSameNewPass) {
					return res.send("Old Password and New Password is the same, Choose New Password");
				} else {
					return User.findByIdAndUpdate(req.user.id, update).then(updated => res.send('Password successfully changed'));
				};
			};
		}).catch(error => res.send(error))
	};

// [SECTION] Functionalities -DELETE
	module.exports.deleteUser = (req, res) => {
		console.log(req.params.id);

		return User.findByIdAndRemove(req.params.id).then(result => res.send('User successfully deleted.')).catch(error => res.send(error));
	};