// [SECTION] Dependencies and Modules
	const User = require('../models/User');
	const Product = require('../models/Product');
	const Order = require('../models/Order');
	const Cart = require('../models/Cart')
	const auth = require('../auth');

// [SECTION] Functionality - CREATE

// [SECTION] Functionality - RETRIEVE
	module.exports.orders = async (req, res) => {
		console.log(req.user.id);
			
		const user = req.user.id;

		const order = await Order.find({user: user}).sort({date: -1})
		if(order){
			return res.status(200).send(order);
		} else {
			return res.send(false);
		}
	};

// [SECTION] Functionality - UPDATE
	// module.exports.payment = (req, res) => {
	// 	if (req.user.isAdmin){
	// 		return res.send({message:'Action Denied'});
	// 	}

	// 	let id = req.params.id;

	// 	let update = {
	// 		isPaid: true
	// 	};
	// 	Order.findByIdAndUpdate(id, update).then(result => res.send({message: 'Successfully Paid'})).catch(error => res.send(error));
	// };

// [SECTION] Functionality - DELETE