// [SECTION] Dependencies And Modules
	const express = require('express');
	const controller = require('../controllers/carts');
	const auth = require('../auth');

	const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] Routes - POST
	route.post('/add', verify, controller.addToCart);

// [SECTION] Routes - GET
	route.get('/cart', verify, controller.cart);

// [SECTION] Routes - PUT
	route.put('/:id/checkOut', verify, controller.checkOut)

// [SECTION] Routes - DELETE
	route.delete('/:id/delete', verify, controller.deleteItem);

// [SECTION] Expose Route System
	module.exports = route;