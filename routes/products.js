// [SECTION] Dependencies and Modules
	const express = require("express");
	const controller = require('../controllers/products');
	const auth = require('../auth');

	const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] Routes - POST
	route.post('/create', verify, verifyAdmin, controller.createProduct);

// [SECTION] Routes - GET
	route.get('/allProducts', controller.getAllProducts);

	route.get('/:id', controller.getSingleProduct);

	route.get('/', controller.getAvailableProducts);

	route.get('/category/mouse', controller.categoryMouse);

	route.get('/category/keyboard', controller.categoryKeyboard);

	route.get('/category/audio', controller.categoryAudio);

	route.get('/category/accessories', controller.categoryAccessories);

// [SECTION] Routes - PUT
	route.put('/:id', verify, verifyAdmin, controller.updateProduct);

	route.put('/:id/archive', verify, verifyAdmin, controller.archiveProduct);

	route.put('/:id/reactivate', verify, verifyAdmin, controller.reactivateProduct);

// [SECTION] Routes - DELETE
	route.delete('/:id/delete', verify, verifyAdmin, controller.deleteProduct);

// [SECTION] Export Route System
	module.exports = route;