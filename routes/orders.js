// [SECTION] Dependencies And Modules
	const express = require('express');
	const controller = require('../controllers/orders');
	const auth = require('../auth');

	const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] Routes - POST
	
// [SECTION] Routes - GET
	route.get('/', verify, controller.orders);

// [SECTION] Routes - PUT

// [SECTION] Routes - DELETE

// [SECTION] Expose Route System
	module.exports = route;