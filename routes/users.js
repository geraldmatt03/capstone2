// [SECTION] Dependencies and Modules
	const express = require("express");
	const controller = require('./../controllers/users');
	const auth = require('../auth');

	const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] Routes - POST
	route.post('/register', controller.registerUser);

	route.post('/login', controller.loginUser);

// [SECTION] Routes - GET
	route.get('/allUsers', verify, verifyAdmin, controller.getAllUsers);

	route.get('/allAdmin', verify, verifyAdmin, controller.getAllAdmin);

	route.get('/allCustomers', verify, controller.getAllCustomers);

	route.get('/getUserDetails', verify, controller.getUserDetails);

// [SECTION] Routes - PUT
	route.put('/:id/setAdmin', verify, verifyAdmin, controller.setAdmin);

	route.put('/updateUser', verify, controller.updateUser);

	route.put('/updatePassword', verify, controller.updatePassword);

// [SECTION] Routes - DELETE
	route.delete('/:id/deleteUser', verify, verifyAdmin, controller.deleteUser);

// [SECTION] Expose Route System
	module.exports = route;